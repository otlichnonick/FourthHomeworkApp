//
//  SecondHomeworkAppApp.swift
//  SecondHomeworkApp
//
//  Created by Anton Agafonov on 05.07.2022.
//

import SwiftUI

@main
struct SecondHomeworkAppApp: App {
    @StateObject var viewModel: ViewModel = .init()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(viewModel)
        }
    }
}
